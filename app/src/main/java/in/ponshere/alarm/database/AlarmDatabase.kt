package `in`.ponshere.alarm.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase


/**
 *
 * @author Ponsuyambu
 * @since  8/7/18.
 */

@Database(entities = [(Alarm::class)], version = 1)
abstract class AlarmDatabase : RoomDatabase() {
    @Volatile
    private var INSTANCE: AlarmDatabase? = null

    companion object {
        var INSTANCE: AlarmDatabase? = null

        fun getInstance(context: Context): AlarmDatabase? {
            if (INSTANCE == null) {
                synchronized(AlarmDatabase::class.java) {
                    if (INSTANCE == null) {
                        INSTANCE = Room.databaseBuilder(context.applicationContext,
                                AlarmDatabase::class.java, "Sample.db")
                                .build()
                    }
                }
            }
            return INSTANCE
        }
    }

    abstract fun alarmDao(): AlarmDao


}