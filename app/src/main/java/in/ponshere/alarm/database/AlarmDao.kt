package `in`.ponshere.alarm.database

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

/**
 *
 * @author Ponsuyambu
 * @since  8/7/18.
 */
@Dao
interface AlarmDao {

    /* Get the user from the table. Since for simplicity we only have one user in the database,
    * this query gets all users from the table, but limits the result to just the 1st user.
    *
    * @return the user from the table
    */
//    @Query("SELECT * FROM ALARM_ENTRIES")
//    fun getAlarms(): Flowable<List<Alarm>>

    @Query("SELECT * FROM ALARM_ENTRIES")
    fun getAlarms(): LiveData<List<Alarm>>

    /**
     * Insert a user in the database. If the user already exists, replace it.
     *
     * @param alarm the user to be inserted.
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAlarm(alarm: Alarm): Long

    @Query("SELECT MAX(id) FROM ALARM_ENTRIES")
    fun getMaxId(): Long

    /**
     * Delete all users.
     */
    @Query("DELETE FROM ALARM_ENTRIES")
    fun deleteAllAlarms()

}