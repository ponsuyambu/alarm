package `in`.ponshere.alarm.database

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 *
 * @author Ponsuyambu
 * @since  8/7/18.
 */
@Entity(tableName = "alarm_entries")
class Alarm  {
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    var id:Int? = null

    @ColumnInfo(name = "title") var title: String = ""
    @ColumnInfo(name = "time") var time: String = ""
    @ColumnInfo(name = "enabled") var isEnabled: Boolean = false

    constructor(title: String, time: String, isEnabled: Boolean) {
        this.title = title
        this.time = time
        this.isEnabled = isEnabled
    }
}