package `in`.ponshere.alarm.datasource

import `in`.ponshere.alarm.database.Alarm
import androidx.lifecycle.LiveData

/**
 *
 * @author Ponsuyambu
 * @since  8/7/18.
 */
interface IAlarmDataSource {
    fun getAlarms() : LiveData<List<Alarm>>
    fun insertAlarm(alarm: Alarm): LiveData<Long>
    fun getMaxId(): LiveData<Long>
}
