package `in`.ponshere.alarm.datasource

import `in`.ponshere.alarm.database.Alarm
import `in`.ponshere.alarm.database.AlarmDao
import `in`.ponshere.alarm.executors.AppExecutors
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData

/**
 *
 * @author Ponsuyambu
 * @since  8/7/18.
 */
class AlarmLocalDataSource(var alarmDao: AlarmDao, var appExecutors: AppExecutors): IAlarmDataSource {
    override fun getMaxId(): LiveData<Long> {
        val liveData =  MutableLiveData<Long>()
        appExecutors.networkIO().execute {
            liveData.postValue(alarmDao.getMaxId())
        }
        return liveData
    }

    override fun getAlarms(): LiveData<List<Alarm>> {
        return alarmDao.getAlarms()
    }

    fun insertOrUpdateAlarm(alarm: Alarm){
        alarmDao.insertAlarm(alarm)
    }

    override fun insertAlarm(alarm: Alarm): LiveData<Long> {
        val liveData =  MutableLiveData<Long>()
        appExecutors.networkIO().execute {
            liveData.postValue(alarmDao.insertAlarm(alarm))
        }
        return liveData
    }
}
