package `in`.ponshere.alarm.contracts

import `in`.ponshere.alarm.database.Alarm
import androidx.lifecycle.LifecycleObserver

/**
 *
 * @author Ponsuyambu
 * @since  8/7/18.
 */
interface IAlarmListContracts {
     interface IAlarmListView {
         fun updateAlarmList(items: List<Alarm>?)
         fun showToast(message: String)
     }

    interface IAlarmListPresenter : LifecycleObserver {
        fun retrieveAll()
        fun unsubscribe()
        fun loadAlarmItems()
        fun insertAlarm(alarm: Alarm)
    }
}