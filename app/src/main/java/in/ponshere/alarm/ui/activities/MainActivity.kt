package `in`.ponshere.alarm.ui.activities

import `in`.ponshere.alarm.R
import `in`.ponshere.alarm.contracts.IAlarmListContracts
import `in`.ponshere.alarm.database.Alarm
import `in`.ponshere.alarm.database.AlarmDatabase
import `in`.ponshere.alarm.datasource.AlarmLocalDataSource
import `in`.ponshere.alarm.executors.AppExecutors
import `in`.ponshere.alarm.presenters.AlarmPresenter
import `in`.ponshere.alarm.ui.adapter.Alarmdapter
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), IAlarmListContracts.IAlarmListView {
    override fun showToast(message: String) {
        Toast.makeText(this, message,Toast.LENGTH_SHORT).show()
    }

    var items = arrayListOf<Alarm>()
    lateinit var alarmAdapter: Alarmdapter
    lateinit var presenter: IAlarmListContracts.IAlarmListPresenter
    lateinit var dataSource: AlarmLocalDataSource

    override fun updateAlarmList(items: List<Alarm>?) {
        this.items.clear()
        this.items.addAll(items!!)
        alarmAdapter.notifyDataSetChanged()

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        fabAdd.setOnClickListener {
            presenter.insertAlarm(Alarm("One","7:00",true))
        }
        var database = AlarmDatabase.getInstance(this)
        dataSource = AlarmLocalDataSource(database!!.alarmDao(), AppExecutors())
        presenter = AlarmPresenter(this, dataSource, this)
        alarmAdapter = Alarmdapter(items, this)
        rvAlarmList.layoutManager = LinearLayoutManager(this)
        rvAlarmList.adapter = alarmAdapter
        lifecycle.addObserver(presenter)

    }

    override fun onDestroy() {
        super.onDestroy()
        lifecycle.removeObserver(presenter)
    }
}
