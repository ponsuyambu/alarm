package `in`.ponshere.alarm.ui.adapter

import `in`.ponshere.alarm.R
import `in`.ponshere.alarm.database.Alarm
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.list_alarm_item.view.*

/**
 *
 * @author Ponsuyambu
 * @since  8/7/18.
 */
class Alarmdapter(val items: List<Alarm>, val context: Context) : RecyclerView.Adapter<Alarmdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.list_alarm_item, parent, false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItems(items[position])
        holder.itemView.swtAlarm.setOnCheckedChangeListener{btn, isChecked ->
            items[holder.adapterPosition].isEnabled = isChecked
        }
    }

    override fun getItemCount(): Int {
        return items.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindItems(item: Alarm) {
            itemView.lblAlarmTime.text = item.time
            itemView.lblAlarmTitle.text = item.title +" "+ item.id
            itemView.swtAlarm.isChecked = item.isEnabled
        }
    }
}