package `in`.ponshere.alarm.presenters

import `in`.ponshere.alarm.contracts.IAlarmListContracts
import `in`.ponshere.alarm.database.Alarm
import `in`.ponshere.alarm.datasource.IAlarmDataSource
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import androidx.lifecycle.OnLifecycleEvent
import io.reactivex.disposables.CompositeDisposable


/**
 *
 * @author Ponsuyambu
 * @since  8/7/18.
 */
class AlarmPresenter(var alarmListView: IAlarmListContracts.IAlarmListView, var alarmDataSource: IAlarmDataSource, var lifecycleOwner: LifecycleOwner) : IAlarmListContracts.IAlarmListPresenter {

    private val compositeDisposable: CompositeDisposable? = null


    init {

    }
    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    fun onCreate(){
        retrieveAll()
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    fun onPause(){
        unsubscribe()
    }

    override fun retrieveAll() {
       // alarmDataSource.getAlarms()
        alarmDataSource.getAlarms().observe(lifecycleOwner, Observer {
            alarmListView.updateAlarmList(it)
        })

        alarmDataSource.getMaxId().observe(lifecycleOwner, Observer<Long> { t -> alarmListView.showToast(t.toString()) })

//        val disposable = alarmDataSource.getAlarms()
//                .subscribeOn(schedulerProvider.io())
//                .observeOn(schedulerProvider.ui())
//                .subscribe(
//                    { result -> alarmListView.updateAlarmList(result) },
//                    { error -> showError(error.message) }
//                )
//        compositeDisposable?.add(disposable)
    }

    private fun showError(message: String?) {

    }

    private fun showResult(result: List<Alarm>?) {
        alarmListView.updateAlarmList(result)
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    override fun unsubscribe() {
        compositeDisposable?.clear()
    }

    override fun loadAlarmItems() {

    }

    override fun insertAlarm(alarm: Alarm) {
        alarmDataSource.insertAlarm(alarm).observe(lifecycleOwner, Observer { retrieveAll() })
    }
}