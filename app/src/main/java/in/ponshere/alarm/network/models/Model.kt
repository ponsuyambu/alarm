package `in`.ponshere.alarm.network.models

/**
 *
 * @author Ponsuyambu
 * @since  7/7/18.
 */
object Model {
    data class Result(val query: Query)
    data class Query(val searchinfo: SearchInfo, val search: List<Search>)
    data class SearchInfo(val totalhits: Int)
    data class Search(val ns:Int, val title: String, val pageId: Int, val snippet: String)
}