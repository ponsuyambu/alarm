package `in`.ponshere.alarm.util

import io.reactivex.Scheduler



/**
 *
 * @author Ponsuyambu
 * @since  8/7/18.
 */
interface BaseSchedulerProvider {
    fun computation(): Scheduler

    fun io(): Scheduler

    fun ui(): Scheduler

}