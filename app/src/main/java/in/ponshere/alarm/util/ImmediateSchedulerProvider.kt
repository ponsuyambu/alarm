package `in`.ponshere.alarm.util

import io.reactivex.Scheduler
import io.reactivex.schedulers.Schedulers



/**
 *
 * @author Ponsuyambu
 * @since  8/7/18.
 */
object ImmediateSchedulerProvider: BaseSchedulerProvider {

    override fun computation(): Scheduler {
        return Schedulers.trampoline()
    }

    override fun io(): Scheduler {
        return Schedulers.trampoline()
    }

    override fun ui(): Scheduler {
        return Schedulers.trampoline()
    }
}