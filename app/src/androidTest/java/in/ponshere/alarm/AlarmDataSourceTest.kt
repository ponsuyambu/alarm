package `in`.ponshere.alarm

import `in`.ponshere.alarm.database.Alarm
import `in`.ponshere.alarm.database.AlarmDao
import `in`.ponshere.alarm.database.AlarmDatabase
import `in`.ponshere.alarm.datasource.AlarmLocalDataSource
import `in`.ponshere.alarm.executors.SingleExecutors
import android.util.Log
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room
import androidx.test.InstrumentationRegistry
import androidx.test.runner.AndroidJUnit4
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.junit.runner.RunWith
import java.io.IOException






/**
 *
 * @author Ponsuyambu
 * @since  14/7/18.
 */
@RunWith(AndroidJUnit4::class)
class AlarmDataSourceTest {
    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()
    private var alarmDao: AlarmDao? = null
    private var alarmDatabase: AlarmDatabase? = null

    private var alarmLocalDataSource: AlarmLocalDataSource? = null


    @Before
    fun createDb(){
        val context = InstrumentationRegistry.getTargetContext()
        alarmDatabase = Room.inMemoryDatabaseBuilder(context, AlarmDatabase::class.java)
                .allowMainThreadQueries()
                .build()
        alarmDao = alarmDatabase?.alarmDao()
        alarmLocalDataSource = AlarmLocalDataSource(alarmDao!!, SingleExecutors())
    }

    @After
    @Throws(IOException::class)
    fun closeDb() {
        alarmDatabase?.close()
    }

    @Test
    fun testInsertion(){

        var t = alarmLocalDataSource?.getMaxId()
        var maxId = t?.value

        Log.d("Testing","MaxId $maxId")

        var insertedId = alarmLocalDataSource?.insertAlarm(Alarm("Test","7:00",true))
        assert(insertedId?.value == maxId!! + 1)

        var alarms = alarmLocalDataSource?.getAlarms()
        assert(alarms?.value?.get(0)?.title.equals("Test")!!)
    }

}